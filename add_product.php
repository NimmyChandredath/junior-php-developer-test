<div class="add_product">
    <nav class="navbar navbar-expand-md  fixed-top">
         <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <div class="navbar-btn btn-md mr-auto">
                      <p> <u> <h4>Add Product </h4> </u> </p>
                    </div>
     </div>
        <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
            <div class="navbar-nav ml-auto">
                 
                <a href="product_list.php">click here to see the  Produt List</a> 

            </div>
         </div>
       
        
</nav>
</div>
<?php
require ("header.php");
$servername = "localhost";
$username = "root";
$password = "rootdb";
$dbname = "tst";

// Create connection
$conn = mysqli_connect($servername, $username, $password,  $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
} 
$sku = "";
$prdname = "";
$prdprice = "";
$dvd = "";
$kg = "";
$ht = "";
$wt = "";
$lt = "";
if (isset($_POST['save'])) {
    $sku = $_POST['sku'];
    $prdname = $_POST['prdname'];
    $prdprice = $_POST['prdprice'];
    $prod_detailsone = "INSERT INTO products_details(sku, prod_name, prod_price)
                        VALUES ('$sku' , '$prdname' , '$prdprice')";
        if (mysqli_query($conn, $prod_detailsone)){
             $pid = mysqli_insert_id($conn);
            echo "new record created Sucessfully"; 
            }else{
                    echo "Error: " . $prod_detailsone . "<br>" . mysqli_error($conn);
                 }
     if(!empty($_POST['dvd'])){
        global $pid;
        $dvd = $_POST['dvd'];
        $prod_detailstwo = "INSERT INTO products_dvd(pid, dvd_size)
                            VALUES ( '$pid' , '$dvd')";
        if (mysqli_query($conn, $prod_detailstwo)){
            }else{
                    echo "Error: " . $prod_detailstwo . "<br>" . mysqli_error($conn);
                 }

    }
    if(!empty($_POST['kg'])){
        global $pid;
        $kg = $_POST['kg'];
        $prod_detailsthree = "INSERT INTO products_book(pid, book_weight)
                              VALUES ('$pid' , '$kg')";
        if (mysqli_query($conn, $prod_detailsthree)){               
            }else{
                    echo "Error: " . $prod_detailsthree . "<br>" . mysqli_error($conn);
                 }

    }
     if((!empty($_POST['ht'])) && (!empty($_POST['wt'])) && (!empty($_POST['lt']))) {
        global $pid;
        $ht = $_POST['ht'];
        $wt = $_POST['wt'];
        $lt = $_POST['lt'];
        $prod_detailsfour = "INSERT INTO products_furniture(pid, furt_height, furt_width, furt_length)
                             VALUES ('$pid' , '$ht' , '$wt', '$lt')";
        if (mysqli_query($conn, $prod_detailsfour)){               
            }else{
                    echo "Error: " . $prod_detailsfour . "<br>" . mysqli_error($conn);
                 }

    }
    
}
mysqli_close($conn);
Add_Product();
function Add_Product(){
?>
<div class="container col-md-3 form-group" style="margin-top: 60px; margin-bottom: 1px;">
    
    <form method="post">
        <div class="" align="left">
            <label for="sku">SKU:</label>
            <input type="text" class="form-control" name="sku" id="sku" required>
        </div>
        <div  align="left">
            <label for="prdname">Name:</label>
            <input type="text" name="prdname" id="prdname" class="form-control" required>
        </div>
        <div align="left">
            <label for="prdprice">Price:</label>
            <input type="text" name="prdprice" class="form-control" id="prdprice" required>
        </div> 
        <br>
        <div align="left">
            <select name="type" id="target" class="dropdown">
                <option value="">Type Switcher...</option>
                <option value="dvd">DVD-disc</option>
                <option value="book">Book</option>
                <option value="furniture">Furniture</option>
            </select>
        </div>
        <br>
        <div id="dvd" class="inv" >
            <label for="mb">Size</label>
            <input type="text" name="dvd" class="form-control" id="mb">
        </div>
        <div id="book" class="inv" >
            <label for="kg">Weight</label>
            <input type="text" name="kg" class="form-control" id="kg">
        </div>
        <div  id="furniture" class="inv" >
            <label for="ht">Height</label>
            <input type="text" name="ht" class="form-control" id="ht"><br>
            <label for="wt">Width</label>
            <input type="text" name="wt" class="form-control" id="wt"><br>
            <label for="lt">Length</label>
            <input type="text" name="lt" class="form-control" id="lt"><br>
        </div>
        <br>
        <div>
            <button type="submit" name= "save" class="btn btn-secondary ml-5" >Save</button>

        </div>
</form> 
</div>
<?php 
}
?>
<script>
    document
        .getElementById('target')
        .addEventListener('change', function () {
            'use strict';
            var vis = document.querySelector('.vis'),
            target = document.getElementById(this.value);
            if (vis !== null) {
                vis.className = 'inv';
            }
            if (target !== null) {
                target.className = 'vis';
            }
        });
</script>
   

