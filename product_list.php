<div class="list_product">
    <nav class="navbar navbar-expand-md  fixed-top">
    	 <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
          <div class="navbar-btn btn-md mr-auto">
            <p><u><h4>Product List</h4></u></p>
          </div>
        </div>
        <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
            <div class="navbar-nav ml-auto">
              <select id="mass" name="mass"  class="form-control mr-3 ml-3">
                <option selected>Mass DeleteAction</option>
                <option>Delete</option>
              </select>
              <button class=" btn btn-primary mr-3" type="submit" name="delete">Apply</button> 
              <a href="add_product.php">Add_Prdt</a> 
            </div>
        </div>       
    </nav>
</div>
<?php
  require ("header.php");
  $servername = "localhost";
  $username = "root";
  $password = "rootdb";
  $dbname = "tst";
  // Create connection
  $conn = mysqli_connect($servername, $username, $password,  $dbname);
  // Check connection
  if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
  } 
  $sql_dvd = "SELECT id, sku, prod_name, prod_price,  dvd_size  
              FROM products_details,  products_dvd
              WHERE products_details.pid = products_dvd.pid";      
  $sql_book = "SELECT  sku, prod_name, prod_price, id, book_weight
  	           FROM products_details, products_book 
               WHERE products_details.pid = products_book.pid";
  $sql_furt = "SELECT  sku, prod_name, prod_price, id, furt_height, furt_width, furt_length
  	           FROM products_details, products_furniture
               WHERE products_details.pid = products_furniture.pid";
  $rslt_dvd = mysqli_query($conn , $sql_dvd);
  if (mysqli_num_rows($rslt_dvd) > 0) {
  	while($row = mysqli_fetch_assoc($rslt_dvd)) {
  ?>  
      <form method="post" action="">
        <div class="container" style="display: block;">
          <div class="card" style="float: left; margin-left: 5px; margin-right: 25px; padding: 20px 20px; width: 230px; margin-top: 60px; margin-bottom: 1px; text-align: center;">    
            <?php
              echo '<input type="checkbox" id="checkAl" name="checkbox[]" value="'. $row["id"] .'">';
            	echo $row["sku"]. "<br>"; 
            	echo $row["prod_name"]. "<br>" ; 
            	echo $row["prod_price"]. "$"."<br>"; 
            	echo "Size:". $row["dvd_size"] . "MB" . "<br>" ;
          echo "</div>";
        echo "</div>";
      echo "</form>";
    } 
  } else{
      print $mysqli_error();
    } 
          ?>
<br>
<br>
<?php
  $rslt_book = mysqli_query($conn , $sql_book);
  if (mysqli_num_rows($rslt_book) > 0) {
  	while($row = mysqli_fetch_assoc($rslt_book)) {
  ?> 
      <form method="post" action="">
        <div class="container" style="display: block; ">
          <div  class="card" style="float: left; margin-left: 5px; margin-right: 25px; padding: 20px 20px; width: 230px; margin-top:60px; margin-bottom: 1px; text-align: center;"> 
            <?php
              echo '<input type="checkbox" id="checkAl" name="checkbox[]" value="'. $row["id"] .'" >';	
            	echo $row["sku"]. "<br>"; 
            	echo $row["prod_name"]. "<br>" ; 
            	echo $row["prod_price"]. "$"."<br>"; 
            	echo "Weight:". $row["book_weight"] . "KG" . "<br>" ;
          echo "</div>";
        echo "</div>";
      echo "</form>";  
    } 
  } else {
      print $mysqli_error();
    }
          ?>
<br>
<br>
<?php
  $rslt_furt = mysqli_query($conn , $sql_furt);
  if (mysqli_num_rows($rslt_furt) > 0) {
  	while($row = mysqli_fetch_assoc($rslt_furt)) {
?> 
      <form method="post" action="">
        <div class="container" style="display: block; width:">
          <div class="card" style="float: left; margin-left: 5px; margin-right: 25px;  padding: 20px 20px; width: 230px; margin-top:60px; margin-bottom: 1px; text-align: center;"> 	  
            <?php
              echo '<input type="checkbox" id="checkAl" name="checkbox[]" value="'. $row["id"] .'" >';
            	echo $row["sku"]. "<br>"; 
            	echo $row["prod_name"]. "<br>" ; 
            	echo $row["prod_price"]. "$"."<br>"; 
            	echo "Dimension:". $row["furt_height"] . "x" . $row["furt_width"]. "x" .  $row["furt_length"]. " " . "<br>" ;
          echo "</div>";
        echo "</div>";
      echo "</form>"; 
    }
  } else {
        print $mysqli_error();
    }
          ?>
<br>
<br>
<?php
$checkbox ="";
$checkbox = $_POST['checkbox'];
if(isset($_POST['delete'])) {
  for ($i=0; $i <count($checkbox) ; $i++) { 
    $del_id = $checkbox[$i];
    echo "hH" .$del_id;
    mysqli_query($conn , "DELETE FROM products_dvd,products_book,products_furniture WHERE pid ='".$del_id."'");
  }
}
mysqli_close($conn);
?>

