-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 14, 2019 at 08:42 PM
-- Server version: 8.0.16
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tst`
--

-- --------------------------------------------------------

--
-- Table structure for table `products_book`
--

CREATE TABLE `products_book` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `book_weight` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products_book`
--

INSERT INTO `products_book` (`id`, `pid`, `book_weight`) VALUES
(1, 2, 1),
(2, 4, 6),
(3, 11, 3),
(4, 12, 5);

-- --------------------------------------------------------

--
-- Table structure for table `products_details`
--

CREATE TABLE `products_details` (
  `pid` int(11) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `prod_name` text NOT NULL,
  `prod_price` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products_details`
--

INSERT INTO `products_details` (`pid`, `sku`, `prod_name`, `prod_price`) VALUES
(1, 'F1DISC', 'Acme', 100),
(2, 'Book1', 'Fire', 20),
(3, 'Furt1', 'Chair', 500),
(4, 'second', 'wings', 6),
(5, 'dvd2', 'Floppy', 9),
(6, 'furt2', 'Desk', 6),
(9, 'dvd3', 'dvdt', 4),
(10, 'dvd4', 'dvd2', 6),
(11, 'book4', 'hhhh', 4),
(12, 'book3', 'kgh', 5),
(13, 'fuc5', 'dccc', 5),
(14, 'furt4', 'dghh', 5);

-- --------------------------------------------------------

--
-- Table structure for table `products_dvd`
--

CREATE TABLE `products_dvd` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `dvd_size` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products_dvd`
--

INSERT INTO `products_dvd` (`id`, `pid`, `dvd_size`) VALUES
(1, 1, 700),
(2, 5, 8),
(5, 9, 5),
(6, 10, 5);

-- --------------------------------------------------------

--
-- Table structure for table `products_furniture`
--

CREATE TABLE `products_furniture` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `furt_height` int(20) NOT NULL,
  `furt_width` int(20) NOT NULL,
  `furt_length` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products_furniture`
--

INSERT INTO `products_furniture` (`id`, `pid`, `furt_height`, `furt_width`, `furt_length`) VALUES
(1, 3, 7, 8, 6),
(2, 6, 2, 8, 1),
(3, 13, 5, 5, 5),
(4, 14, 4, 5, 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products_book`
--
ALTER TABLE `products_book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_details`
--
ALTER TABLE `products_details`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `products_dvd`
--
ALTER TABLE `products_dvd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_furniture`
--
ALTER TABLE `products_furniture`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products_book`
--
ALTER TABLE `products_book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `products_details`
--
ALTER TABLE `products_details`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `products_dvd`
--
ALTER TABLE `products_dvd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `products_furniture`
--
ALTER TABLE `products_furniture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
